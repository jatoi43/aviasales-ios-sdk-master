//
//  ASTAppDelegate.h
//
// Copyright 2019 Compare The Hotel Ltd.
//


#import <UIKit/UIKit.h>

@interface JRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
