//
//  UIViewController+JRAddChildViewController.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIViewController (JRAddChildViewController)

- (void)addChildViewController:(UIViewController *)childController toView:(UIView *)view;
- (void)deleteChildViewController:(UIViewController *)viewController;

@end
