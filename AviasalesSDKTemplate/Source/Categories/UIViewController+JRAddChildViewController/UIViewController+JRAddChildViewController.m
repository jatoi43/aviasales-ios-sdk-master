//
//  UIViewController+JRAddChildViewController.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "UIViewController+JRAddChildViewController.h"

@implementation UIViewController (JRAddChildViewController)

- (void)addChildViewController:(UIViewController *)childController toView:(UIView *)view {
    if (!childController || !view) {
        return;
    }
    if (childController.parentViewController) {
        [self deleteChildViewController:childController];
    }
    [self addChildViewController:childController];
    [view addSubview:childController.view];
    [childController didMoveToParentViewController:self];
}

- (void)deleteChildViewController:(UIViewController *)viewController {
    if (!viewController) {
        return;
    }
    [viewController willMoveToParentViewController:nil];
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
}

@end
