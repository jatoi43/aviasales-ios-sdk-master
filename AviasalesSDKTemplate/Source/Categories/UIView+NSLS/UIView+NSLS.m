//
//  UIView+NSLS.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "UIView+NSLS.h"

@implementation UIView (NSLS)

- (NSString *)NSLSKey {
    return nil;
}

- (void)setNSLSKey:(NSString *)NSLSKey
{
    NSString *localizedString = NSLS(NSLSKey);
    id strongSelf = self;
    if ([strongSelf isKindOfClass:[UILabel class]]) {
        UILabel *label = strongSelf;
        [label setText:localizedString];
    } else if ([strongSelf isKindOfClass:[UIButton class]]) {
        UIButton *button = strongSelf;
        [button setTitle:localizedString forState:UIControlStateNormal];
    }
}

@end
