//
//  UIView+NSLS.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIView (NSLS)

@property (nonatomic, strong) IBInspectable NSString *NSLSKey;

@end
