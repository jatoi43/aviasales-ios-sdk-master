//
//  UINavigationController+Additions.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "UINavigationController+Additions.h"

@implementation UINavigationController(Additions)

- (void)replaceTopViewControllerWith:(UIViewController *)viewController {
    NSMutableArray *const viewControllers = [self.viewControllers mutableCopy];
    [viewControllers removeLastObject];
    [viewControllers addObject:viewController];
    [self setViewControllers:viewControllers animated:YES];
}

- (void)setViewControllersWithRootAndViewController:(UIViewController *)viewController {
    NSArray <UIViewController *> *viewControllers = @[self.viewControllers.firstObject, viewController];
    [self setViewControllers:viewControllers animated:YES];
}

@end
