//
//  UINavigationController+Additions.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UINavigationController(Additions)

- (void)replaceTopViewControllerWith:(UIViewController *)viewController;
- (void)setViewControllersWithRootAndViewController:(UIViewController *)viewController;

@end
