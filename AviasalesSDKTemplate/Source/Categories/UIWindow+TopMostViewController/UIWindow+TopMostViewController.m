//
//  UIApplication+TopMostViewController.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "UIWindow+TopMostViewController.h"

@implementation UIWindow (TopMostViewController)

- (UIViewController*)topMostController
{
    UIViewController *topController = [self rootViewController];
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (BOOL)topMostControllerIsModal
{
    if ([self topMostController].presentingViewController != nil) {
        return YES;
    }
    return NO;
}

@end
