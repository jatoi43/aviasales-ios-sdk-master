//
//  UIApplication+TopMostViewController.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIWindow (TopMostViewController)

- (UIViewController *)topMostController;
- (BOOL)topMostControllerIsModal;

@end
