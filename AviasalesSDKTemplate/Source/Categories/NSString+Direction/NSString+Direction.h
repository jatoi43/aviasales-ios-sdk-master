//
//  NSString+Direction.h
//  Compare The Hotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>

@interface NSString (Direction)

- (NSString *)rtlStringIfNeeded;

- (NSString *)formatAccordingToTextDirection;

@end
