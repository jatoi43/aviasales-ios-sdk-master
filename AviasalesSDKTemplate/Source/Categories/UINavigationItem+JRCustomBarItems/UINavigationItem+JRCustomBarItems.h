//
//  UINavigationItem+JRCustomBarItems.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

#define kJRCustomBarItemsNegativeSeperatorWidth 10

@interface UINavigationItem (JRCustomBarItems)

+ (UIBarButtonItem *)barItemWithImageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName buttonClass:(Class)buttonClass target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barItemWithImageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barItemWithImageName:(NSString *)imageName target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barItemWithTitle:(NSString *)title target:(id)target action:(SEL)action;

@end
