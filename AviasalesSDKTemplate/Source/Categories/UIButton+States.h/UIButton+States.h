//
//  UIButton+States.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIButton (States)

- (void)setSetupButtonStates:(BOOL)setupButtonStates;

@end
