//
//  UIImage+JRUIImage.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIImage (JRUIImage)

+ (UIImage *)imageWithColor:(UIColor *)color;
- (UIImage *)imageTintedWithColor:(UIColor *)color;
- (UIImage *)imageTintedWithColor:(UIColor *)color fraction:(CGFloat)fraction;
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha;

@end
