//
//  UIView+Nib.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface UIView (Nib)

+ (instancetype)loadFromNib;

@end
