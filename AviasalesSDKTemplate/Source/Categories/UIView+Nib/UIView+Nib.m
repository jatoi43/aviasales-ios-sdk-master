//
//  UIView+Nib.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "UIView+Nib.h"

@implementation UIView (Nib)

+ (instancetype)loadFromNib {
    NSArray <NSString *> *componets = [NSStringFromClass(self) componentsSeparatedByString:@"."];
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:componets.lastObject owner:nil options:nil];
    return [array firstObject];
}

@end
