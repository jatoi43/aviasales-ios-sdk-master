//
//  NSLocale+UILocale.m
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import "NSLocale+UILocale.h"

@implementation NSLocale (UILocale)

+ (NSLocale *)applicationUILocale {
    return [NSLocale localeWithLocaleIdentifier:AVIASALES__(@"LANGUAGE", [NSLocale currentLocale].localeIdentifier)];
}

@end
