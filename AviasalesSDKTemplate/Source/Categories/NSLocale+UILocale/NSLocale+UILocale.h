//
//  NSLocale+UILocale.h
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>

@interface NSLocale (UILocale)

+ (NSLocale *)applicationUILocale;

@end
