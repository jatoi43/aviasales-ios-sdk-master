//
//  CAAnimation+JRPopup.h
//
// Copyright 2019 Compare The Hotel limited
//


#import <Foundation/Foundation.h>


@interface CAAnimation (JRPopup)

+ (CAAnimation *)attachPopUpAnimation;
+ (CAAnimation *)detachPopUpAnimation;

@end
