//
//  NSLayoutConstraint+JRConstraintMake.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (JRConstraintMake)

NSArray <NSLayoutConstraint *> *JRConstraintsMakeScaleToFill(id item,
                                      id toItem);

NSArray <NSLayoutConstraint *> *JRConstraintsMakeEqualSize(id item,
                                    id toItem);

NSLayoutConstraint *JRConstraintMake(id item,
                                     NSLayoutAttribute attribute,
                                     NSLayoutRelation relation,
                                     id toItem,
                                     NSLayoutAttribute secondAttribute,
                                     CGFloat multiplier,
                                     CGFloat constant);
@end
