//
//  UIView+JRFadeAnimation.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

#define kJRFadeAnimationFastTransitionDuration 0.2
#define kJRFadeAnimationMediumTransitionDuration 0.4
#define kJRFadeAnimationLondTransitionDuration 0.6

@interface UIView (JRFadeAnimation)

+ (void)addTransitionFadeToView:(UIView *)view
                       duration:(NSTimeInterval)duration;

@end
