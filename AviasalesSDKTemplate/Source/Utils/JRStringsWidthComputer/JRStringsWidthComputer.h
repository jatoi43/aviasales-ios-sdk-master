//
//  JRStringsWidthComputer.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

@interface JRStringsWidthComputer : NSObject
- (instancetype)initWithFont:(UIFont *)font;
- (CGFloat)widthWithString:(NSString *)string;
@end
