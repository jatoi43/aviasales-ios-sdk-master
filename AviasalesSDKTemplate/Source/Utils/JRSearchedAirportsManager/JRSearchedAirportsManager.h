//
//  JRSearchedAirportsManager.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

@interface JRSearchedAirportsManager : NSObject

+ (void)markSearchedAirport:(JRSDKAirport *)searchedAirport;

+ (NSArray<JRSDKAirport *> *)searchedAirports;

@end
