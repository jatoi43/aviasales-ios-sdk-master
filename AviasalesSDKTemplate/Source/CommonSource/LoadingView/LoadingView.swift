//
//  LoadingView.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

class LoadingView: UIView {

    @IBOutlet var view: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let view = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)?.first as! UIView
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(JRConstraintsMakeScaleToFill(self, view))
    }
}
