//
//  BrowserViewPresenter.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel limited
//

@objc protocol BrowserViewPresenter {
    func handleLoad(view: BrowserViewProtocol)
    func handleClose()
    func handleStartNavigation()
    func handleCommit()
    func handleFinish()
    func handleFail(error: Error)
    func handleFailedURL()
    func handleError()
    func handleGoBack()
    func handleGoForward()
}
