//
//  JRNavigationController.h
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <UIKit/UIKit.h>

#define kJRNavigationControllerDefaultTextSize 17

@interface JRNavigationController : UINavigationController

@property (nonatomic) BOOL allowedIphoneAutorotate;

- (void)removeAllViewControllersExceptCurrent;
- (void)setupNavigationBar;

@end
