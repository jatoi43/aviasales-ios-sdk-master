//
//  JRViewController.h
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <UIKit/UIKit.h>
#import "UINavigationItem+JRCustomBarItems.h"

#define kJRViewControllerTopHeight 0.5

#define kJRBaseMenuButtonImageName @"JRBaseMenuButton"
#define kJRBaseBackButtonImageName @"back_icon"

@interface JRViewController : UIViewController

@property (nonatomic) BOOL viewIsVisible;

- (BOOL)shouldShowNavigationBar;

@end
