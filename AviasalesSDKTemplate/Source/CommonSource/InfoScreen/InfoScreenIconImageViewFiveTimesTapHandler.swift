//
//  InfoScreenIconImageViewFiveTimesTapHandler.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

protocol InfoScreenIconImageViewFiveTimesTapHandler {
    func handleFiveTimesTap()
}

extension InfoScreenIconImageViewFiveTimesTapHandler {
    func handleFiveTimesTap() {}
}

