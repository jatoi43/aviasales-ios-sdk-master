//
//  InfoScreenVersionCell.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

protocol InfoScreenVersionCellProtocol {
    var version: String? { get }
}

class InfoScreenVersionCell: UITableViewCell {

    @IBOutlet weak var versionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
//        versionLabel.textColor = JRColorScheme.darkTextColor()
        versionLabel.textColor = JRColorScheme.formTextColorCustomColor()
        
    }

    func setup(cellModel: InfoScreenVersionCellProtocol) {
        versionLabel.text = "VERSION COSTA RICA 1.9.1"//cellModel.version
    }
}
