//
//  InfoScreenAboutCell.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

protocol InfoScreenAboutCellProtocol {
    var icon: String { get }
    var logo: String? { get }
    var name: String? { get }
    var description: String? { get }
    var separator: Bool { get }
}

class InfoScreenAboutCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var contentViewBack: UIView!
    
    var iconImageViewFiveTimesTapAction: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setupUI()
    }

    private func setupUI() {
        contentViewBack.backgroundColor = JRColorScheme.formBackgroundColorCustomColor()
//        iconImageView.layer.cornerRadius = 14
//        iconImageView.layer.borderWidth = JRPixel()
//        iconImageView.layer.borderColor = UIColor.lightGray.cgColor
        iconImageView.clipsToBounds = true
//        nameLabel.textColor = JRColorScheme.darkTextColor()
//        descriptionLabel.textColor = JRColorScheme.darkTextColor()
        nameLabel.textColor = JRColorScheme.formTextColorCustomColor()
        descriptionLabel.textColor = JRColorScheme.formTextColorCustomColor()
    }

    func setup(cellModel: InfoScreenAboutCellProtocol) {
        let appIcon =  UIImage(named: "compareHotelLogo") //UIImage(named: cellModel.icon)
        if let logo = cellModel.logo, let url = URL(string: logo) {
            iconImageView.sd_setImage(with: url, placeholderImage: appIcon, options: .delayPlaceholder)
        } else {
            iconImageView.image = appIcon
        }
        nameLabel.text = cellModel.name
        descriptionLabel.text = cellModel.description
        
        nameLabel.isHidden = true
        descriptionLabel.isHidden = true
    }

    @IBAction func iconImageViewFiveTimesTapped(_ sender: UITapGestureRecognizer) {
        iconImageViewFiveTimesTapAction?()
    }
}
