//
//  InfoScreenAboutCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenAboutCellModel: InfoScreenCellModelProtocol, InfoScreenAboutCellProtocol {
    let type = InfoScreenCellModelType.about
    let icon: String
    let logo: String?
    let name: String?
    let description: String?
    let separator: Bool
}
