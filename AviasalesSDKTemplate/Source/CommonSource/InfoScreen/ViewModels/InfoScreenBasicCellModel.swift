//
//  InfoScreenBasicCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenBasicCellModel: InfoScreenCellModelProtocol {
    let type: InfoScreenCellModelType
    let title: String
}
