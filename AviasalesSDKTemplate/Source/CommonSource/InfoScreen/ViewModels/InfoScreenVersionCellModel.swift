//
//  InfoScreenVersionCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenVersionCellModel: InfoScreenCellModelProtocol, InfoScreenVersionCellProtocol {
    let type = InfoScreenCellModelType.version
    let version: String?
}
