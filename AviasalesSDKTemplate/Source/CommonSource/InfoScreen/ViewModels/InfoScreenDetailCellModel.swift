//
//  InfoScreenDetailCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenDetailCellModel: InfoScreenCellModelProtocol {
    let type: InfoScreenCellModelType
    let title: String
    let subtitle: String
}
