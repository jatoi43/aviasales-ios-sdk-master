//
//  InfoScreenExtrnalCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenExtrnalCellModel: InfoScreenCellModelProtocol {
    let type = InfoScreenCellModelType.external
    let name: String?
    let url: String?
}
