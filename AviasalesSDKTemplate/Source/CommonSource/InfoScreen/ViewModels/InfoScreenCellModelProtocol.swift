//
//  InfoScreenCellModelProtocol.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

enum InfoScreenCellModelType {
    case about
    case rate
    case currency
    case email
    case external
    case version
//    case privacy
//    case usage
}

protocol InfoScreenCellModelProtocol {
    var type: InfoScreenCellModelType { get }
}
