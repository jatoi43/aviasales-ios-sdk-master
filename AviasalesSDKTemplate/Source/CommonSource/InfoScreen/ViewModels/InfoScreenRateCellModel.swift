//
//  InfoScreenRateCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct InfoScreenRateCellModel: InfoScreenCellModelProtocol, InfoScreenRateCellProtocol {
    let type = InfoScreenCellModelType.rate
    let name: String?
}
