//
//  JRTableManagerUnion.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTableManager.h"

/**
 * Works with one cell per section
 */
@interface JRTableManagerUnion : NSObject <JRTableManager>

@property (copy, nonatomic) NSIndexSet *secondManagerPositions;

- (instancetype)initWithFirstManager:(id<JRTableManager>)firstManager
                       secondManager:(id<JRTableManager>)secondManager
              secondManagerPositions:(NSIndexSet *)secondManagerPositions;

@end
