//
//  JRTableManager.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@protocol JRTableManager <UITableViewDataSource, UITableViewDelegate>
@end
