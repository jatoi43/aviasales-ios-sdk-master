//
//  CurrencyPickerCellModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct CurrencyPickerCellModel: CurrencyCellModelProtocol {

    let currency: HDKCurrency
    let code: String
    let name: String
    let selected: Bool

    init(currency: HDKCurrency, selectedCurrency: HDKCurrency) {
        self.currency = currency
        self.code = currency.code
        self.name = currency.text
        self.selected = currency == selectedCurrency
    }
}
