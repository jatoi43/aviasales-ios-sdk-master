//
//  CurrencyPickerSectionModel.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

struct CurrencyPickerSectionModel {

    let cellModels: [CurrencyPickerCellModel]

    init(currencies: [HDKCurrency], selectedCurrency: HDKCurrency) {
        cellModels = currencies.map { CurrencyPickerCellModel(currency: $0, selectedCurrency: selectedCurrency) }
    }
}
