//
//  CurrencyCell.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

protocol CurrencyCellModelProtocol {
    var code: String { get }
    var name: String { get }
    var selected: Bool { get }
}

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        codeLabel.textColor = JRColorScheme.darkTextColor()
        nameLabel.textColor = JRColorScheme.darkTextColor()
    }

    func setup(_ cellModel: CurrencyCellModelProtocol) {
        codeLabel.text = cellModel.code
        nameLabel.text = cellModel.name
        update(cellModel)
    }

    func update(_ cellModel: CurrencyCellModelProtocol) {
        accessoryType = cellModel.selected ? .checkmark : .none
    }
}
