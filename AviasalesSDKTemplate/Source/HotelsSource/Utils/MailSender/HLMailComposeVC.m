//
//  HLMailComposeVC.m
//  HotelLook
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import "HLMailComposeVC.h"

@implementation HLMailComposeVC

- (void) viewWillAppear:(BOOL)animated
{
    [self.parentViewController resignFirstResponder];
    [super viewWillAppear:animated];
	
	[self.view setNeedsLayout];
	[self.view layoutIfNeeded];
}

@end
