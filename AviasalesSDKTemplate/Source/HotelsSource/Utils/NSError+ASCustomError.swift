//
//  NSError+ASCustomError.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

extension NSError {

    static func error(code: JRSDKServerAPIError) -> NSError {
        return NSError(domain: "ASErrorDomain", code: code.rawValue, userInfo: nil)
    }
}
