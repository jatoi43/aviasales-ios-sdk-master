//
//  HLScrollView.swift
//  HotelLook
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

class HLScrollView: UIScrollView {

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: self)
        let subview = hitTest(point, with: nil)

        switch subview {
        case is UISlider, is HLRangeSlider:
            return false

        default:
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
    }

}
