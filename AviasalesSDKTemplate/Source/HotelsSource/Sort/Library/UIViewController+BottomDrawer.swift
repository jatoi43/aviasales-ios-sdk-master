//
//  UIViewController+BottomDrawer.swift
//  Compare The Hotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

public extension UIViewController {
    var bottomDrawer: BottomDrawer? {
        if let res = self as? BottomDrawer {
            return res
        }
        return self.parent?.bottomDrawer
    }
}
