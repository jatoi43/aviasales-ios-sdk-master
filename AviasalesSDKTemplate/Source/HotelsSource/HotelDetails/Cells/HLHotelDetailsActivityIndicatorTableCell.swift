//
//  HLActivityIndicatorTableCell.swift
//  HotelLook
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

class HLHotelDetailsActivityIndicatorTableCell: HLHotelDetailsTableCell {
    @IBOutlet weak var indicatorVerticalConstraint: NSLayoutConstraint!

    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!

    // MARK: - Internal methods

    func startAnimating() {
        self.activityIndicator.style = .gray
        self.activityIndicator.startAnimating()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
    }
}
