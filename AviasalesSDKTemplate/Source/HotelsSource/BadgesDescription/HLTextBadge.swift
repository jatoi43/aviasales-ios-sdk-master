//
//  HLTextBadge.swift
//  HotelLook
//
//  
//
//

import UIKit

class HLTextBadge: HLPopularHotelBadge {
    init(name: String, systemName: String, color: UIColor) {
        super.init()
        self.name = name
        self.systemName = systemName
        self.color = color
    }
}
