//
//  TBClusterAnnotation.h
//  TBAnnotationClustering
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface TBClusterAnnotation : NSObject <MKAnnotation>

@property (assign, nonatomic) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (nonatomic, strong) NSArray * variants;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate variants:(NSArray *)variants;

@end
