//
//  TBCoordinateQuadTree.h
//  TBAnnotationClustering
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>
#import "TBQuadTree.h"

@interface TBCoordinateQuadTree : NSObject

@property (assign, nonatomic) TBQuadTreeNode* root;

- (void)buildTreeWithVariants:(NSArray *)variants;
- (NSArray *)clusteredAnnotationsWithinMapRect:(MKMapRect)rect withZoomScale:(double)zoomScale;

@end
