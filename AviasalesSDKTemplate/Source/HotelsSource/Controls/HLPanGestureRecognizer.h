//
//  HLPanGestureRecognizer.h
//  HotelLook
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIGestureRecognizerSubclass.h>


typedef enum {
    HLPanGestureRecognizerDirectionVertical,
    HLPanGestureRecognizerDirectionHorizontal
} HLPanGestureRecognizerDirection;


@interface HLPanGestureRecognizer : UIPanGestureRecognizer

@property (nonatomic, assign) HLPanGestureRecognizerDirection direction;

@end

