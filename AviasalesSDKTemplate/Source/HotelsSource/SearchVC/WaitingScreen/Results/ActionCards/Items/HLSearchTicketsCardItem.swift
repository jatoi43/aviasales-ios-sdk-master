//
//  HLSearchTicketsCardItem.swift
//  CompareTheHotel
//

//  Copyright © 2019 Compare The Hotel Limited.
//

import UIKit

class HLSearchTicketsCardItem: HLActionCardItem {

    override init(topItem: Bool, cellReuseIdentifier: String, filter: Filter?, delegate: HLActionCellDelegate?) {
        super.init(topItem: topItem, cellReuseIdentifier: cellReuseIdentifier, filter: filter, delegate: delegate)
        height = 76.0
    }
}
