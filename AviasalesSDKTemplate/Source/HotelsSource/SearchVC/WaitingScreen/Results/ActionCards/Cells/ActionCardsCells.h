//
//  ActionCardsCell.h
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

#ifndef ActionCardsCell_h
#define ActionCardsCell_h

#import "HLDistanceFilterCardCell.h"
#import "HLNearbyCitiesSearchCardCell.h"
#import "HLPlaceholderCell.h"
#import "HLPriceFilterCardCell.h"
#import "HLRatingCardCell.h"
#import "HLResultCardCell.h"
#import "HLSearchTicketsCardCell.h"

#endif /* ActionCardsCell_h */
