//
//  JRAirportPickerCellWithInformation.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRTableViewCell.h"

@interface JRAirportPickerCellWithInfo : JRTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *locationInfoLabel;

- (void)startActivityIndicator;
- (void)stopActivityIndicator;

@end

