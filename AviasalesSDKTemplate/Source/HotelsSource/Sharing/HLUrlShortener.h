//
//  HLUrlShortener.h
//  HotelLook
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>

#import "HLResultVariant.h"

@interface HLUrlShortener : NSObject

@property (nonatomic, readonly) NSString *longUrlString;
@property (nonatomic, readonly) NSString *shortenedUrlString;

- (void)shortenUrlForVariant:(HLResultVariant *)variant completion:(void (^)(void))completion;
- (NSString *)shortenedUrl;

@end
