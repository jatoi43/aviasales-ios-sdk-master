//
//  FirebaseAnalyticsEngine.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Firebase

class FirebaseAnalyticsEngine: AnalyticsEngine {

    func event(name: String, payload: [String : String]) {
        Analytics.logEvent(name, parameters: payload)
    }
}
