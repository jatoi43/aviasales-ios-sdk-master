//
//  AppearanceSettings.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation
import UIKit

@objcMembers
class AppearanceSettings: NSObject {

    static func setup() {

        UINavigationBar.appearance().barStyle = .black

        UISwitch.appearance().onTintColor = JRColorScheme.mainColor()

        UITableView.appearance().backgroundColor = JRColorScheme.mainBackgroundColor()
        
        UICollectionView.appearance().backgroundColor = JRColorScheme.mainBackgroundColor()
    }
}
