//
//  JRColorScheme.h
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <Foundation/Foundation.h>

@interface JRColorScheme : NSObject

+ (UIColor *)colorFromConstant:(NSString *)textColorConstant;

// Base
+ (UIColor *)mainColor;
+ (UIColor *)actionColor;

// SearchForm
+ (UIColor *)searchFormTintColor;
+ (UIColor *)searchFormBackgroundColor;
+ (UIColor *)searchFormTextColor;

// Background
+ (UIColor *)mainBackgroundColor;
+ (UIColor *)lightBackgroundColor;
+ (UIColor *)darkBackgroundColor;
+ (UIColor *)itemsBackgroundColor;
+ (UIColor *)itemsSelectedBackgroundColor;

// PriceCalendar
+ (UIColor *)priceCalendarBarColor;
+ (UIColor *)priceCalendarSelectedBarColor;
+ (UIColor *)priceCalendarMinBarColor;
+ (UIColor *)priceCalendarSelectedMinBarColor;
+ (UIColor *)priceCalendarMinPriceLevelColor;
+ (UIColor *)priceCalendarResultCellCheapestViewBackgroundColor;

// Slider
+ (UIColor *)sliderBackgroundColor;

//Text
+ (UIColor *)darkTextColor;
+ (UIColor *)lightTextColor;
+ (UIColor *)inactiveLightTextColor;

// Separator
+ (UIColor *)separatorLineColor;

// Button
+ (UIColor *)buttonBackgroundColor;
+ (UIColor *)buttonSelectedBackgroundColor;

// Rating stars
+ (UIColor *)ratingStarDefaultColor;
+ (UIColor *)ratingStarSelectedColor;

// Filters
+ (UIImage *)sliderMinImage;
+ (UIImage *)sliderMaxImage;

+ (UIColor *)photoActivityIndicatorBackgroundColor;
+ (UIColor *)photoActivityIndicatorBorderColor;

// Hotels
+ (UIColor *)hotelBackgroundColor;
+ (UIColor *)discountColor;
+ (UIColor *)priceBackgroundColor;

#pragma mark - Hotel details
+ (UIColor *)ratingColor:(NSInteger)rating;

// From Custom Colors Class
+ (UIColor *)mainColorCustomColor;
//for button color
+ (UIColor *)actionColorCustomColor;
// for upper two buttons
+ (UIColor *)formTintColorCustomColor;
//page background
+ (UIColor *)formBackgroundColorCustomColor;
// text in the app
+ (UIColor *)formTextColorCustomColor;

@end
