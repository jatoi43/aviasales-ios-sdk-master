//
//  BlackColorScheme.swift
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

class BlackColorScheme: NSObject, ColorScheme {
    var mainColor = #colorLiteral(red: 0.2588235294, green: 0.2666666667, blue: 0.2784313725, alpha: 1)
    var actionColor =  #colorLiteral(red: 1, green: 0.6235294118, blue: 0, alpha: 1)
    var formTintColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var formBackgroundColor = #colorLiteral(red: 0.2588235294, green: 0.2666666667, blue: 0.2784313725, alpha: 1)
    var formTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
}
