//
//  ColorScheme.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

@objc protocol ColorScheme: NSObjectProtocol {
    var mainColor: UIColor { get set }
    var actionColor: UIColor { get set }
    var formTintColor: UIColor { get set }
    var formBackgroundColor: UIColor { get set }
    var formTextColor: UIColor { get set }
}

