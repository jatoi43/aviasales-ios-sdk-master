//
//  PurpleColorScheme.swift
//
// Copyright 2019 Compare The Hotel Ltd.
//
//

import UIKit

class PurpleColorScheme: NSObject, ColorScheme {
    var mainColor = #colorLiteral(red: 0.6117647059, green: 0.4235294118, blue: 0.7450980392, alpha: 1)
    var actionColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    var formTintColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var formBackgroundColor = #colorLiteral(red: 0.6117647059, green: 0.4235294118, blue: 0.7450980392, alpha: 1)
    var formTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
}
