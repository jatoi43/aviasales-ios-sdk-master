//
//  CustomColorScheme.swift
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

class CustomColorScheme: NSObject, ColorScheme {
    // for top (under battery icon and so) and under when flight or hotel  selected
    var mainColor = #colorLiteral(red: 0.1215686275, green: 0.1450980392, blue: 0.1843137255, alpha: 1)
    //for button color
    var actionColor =  #colorLiteral(red: 0.9450980392, green: 0.05098039216, blue: 0.3019607843, alpha: 1)
    // for upper two buttons
    var formTintColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    //page background
    var formBackgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1450980392, blue: 0.1843137255, alpha: 1)
    // text in the app
    var formTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
}

