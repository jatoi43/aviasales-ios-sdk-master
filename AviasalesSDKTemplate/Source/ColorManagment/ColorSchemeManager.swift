//
//  ColorSchemeManager.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

@objcMembers
class ColorSchemeManager: NSObject {
    
    static let shared = ColorSchemeManager()
    
//    var current: ColorScheme = BlueColorScheme()
    
    var current: ColorScheme = CustomColorScheme()
}
