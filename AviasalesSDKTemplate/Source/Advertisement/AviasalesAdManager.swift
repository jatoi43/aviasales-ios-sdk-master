//
//  AviasalesAdManager.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

class AviasalesAdManager {

    static let shared = AviasalesAdManager()

    private (set) var cachedAdView: AviasalesSDKAdsView?

    func loadAdView(for searchInfo: JRSDKSearchInfo) {

        cachedAdView = nil

        AviasalesSDK.sharedInstance().adsManager.loadAdsViewForSearchResults(with: searchInfo) { [weak self] (adView, error) in
            self?.cachedAdView = adView
        }
    }
}
