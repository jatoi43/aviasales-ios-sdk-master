//
//  JRDatePickerEnums.h
//
// Copyright 2019 Compare The Hotel limited
//

#ifndef JRDatePickerEnums_h
#define JRDatePickerEnums_h

typedef NS_ENUM(NSInteger, JRDatePickerMode) {
    JRDatePickerModeDeparture,
    JRDatePickerModeReturn,
    JRDatePickerModeDefault
};

#endif /* JRDatePickerEnums_h */
