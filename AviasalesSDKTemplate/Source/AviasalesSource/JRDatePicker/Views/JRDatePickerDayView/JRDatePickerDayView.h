//
//  JRDatePickerDayView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRDatePickerMonthItem.h"

@interface JRDatePickerDayView : UIButton

@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) BOOL todayLabelHidden;
@property (assign, nonatomic) BOOL backgroundImageViewHidden;

- (void)setDate:(NSDate *)date monthItem:(JRDatePickerMonthItem *)monthItem;

@end
