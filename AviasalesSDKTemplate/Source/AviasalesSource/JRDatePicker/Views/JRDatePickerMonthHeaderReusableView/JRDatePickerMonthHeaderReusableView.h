//
//  JRDatePickerMonthHeaderReusableView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRDatePickerMonthItem.h"

@interface JRDatePickerMonthHeaderReusableView : UITableViewHeaderFooterView

@property (strong, nonatomic) JRDatePickerMonthItem *monthItem;

@end
