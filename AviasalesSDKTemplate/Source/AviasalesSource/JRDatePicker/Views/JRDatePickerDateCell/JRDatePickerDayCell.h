//
//  JRDatePickerDayCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRDatePickerMonthItem.h"

@interface JRDatePickerDayCell : UITableViewCell

- (void)setDatePickerItem:(JRDatePickerMonthItem *)datePickerItem dates:(NSArray *)dates;

@end
