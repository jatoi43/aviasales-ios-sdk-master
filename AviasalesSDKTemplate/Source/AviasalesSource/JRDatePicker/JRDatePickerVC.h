//
//  JRDatePickerVC.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRViewController.h"
#import "JRDatePickerEnums.h"

typedef void (^JRDatePickerVCSelecionBlock)(NSDate *selectedDate);

@interface JRDatePickerVC : JRViewController

- (instancetype)initWithMode:(JRDatePickerMode)mode
                  borderDate:(NSDate *)borderDate
                   firstDate:(NSDate *)firstDate
                  secondDate:(NSDate *)secondDate
              selectionBlock:(JRDatePickerVCSelecionBlock)selectionBlock;

@end
