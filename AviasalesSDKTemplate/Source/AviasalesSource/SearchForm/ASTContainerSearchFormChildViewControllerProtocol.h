//
//  ASTContainerSearchFormChildViewControllerProtocol.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

@protocol ASTContainerSearchFormChildViewControllerProtocol <NSObject>

- (void)performSearch;

@end
