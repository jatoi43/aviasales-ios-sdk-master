//
//  ASTSimpleSearchFormViewController.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "ASTContainerSearchFormChildViewControllerProtocol.h"

@interface ASTSimpleSearchFormViewController : UIViewController <ASTContainerSearchFormChildViewControllerProtocol>

- (void)updateSearchInfoWithDestination:(JRSDKAirport *)destination checkIn:(NSDate *)checkIn checkOut:(NSDate *)checkOut passengers:(ASTPassengersInfo *)passengers;

- (void)update;

@end
