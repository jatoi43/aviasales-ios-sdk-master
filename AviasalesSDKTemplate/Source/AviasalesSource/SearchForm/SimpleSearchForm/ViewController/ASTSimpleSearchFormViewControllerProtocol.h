//
//  ASTSimpleSearchFormViewControllerProtocol.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>
#import "JRDatePickerEnums.h"

@class ASTSimpleSearchFormViewModel;

@protocol ASTSimpleSearchFormViewControllerProtocol <NSObject>

- (void)updateWithViewModel:(ASTSimpleSearchFormViewModel *)viewModel;
- (void)showAirportPickerWithType:(ASAirportPickerType)type;
- (void)showDatePickerWithMode:(JRDatePickerMode)mode borderDate:(NSDate *)borderDate firstDate:(NSDate *)firstDate secondDate:(NSDate *)secondDate;
- (void)showPassengersPickerWithInfo:(ASTPassengersInfo *)passengersInfo;
- (void)showErrorWithMessage:(NSString *)message;
- (void)showWaitingScreenWithSearchInfo:(JRSDKSearchInfo *)searchInfо;

@end
