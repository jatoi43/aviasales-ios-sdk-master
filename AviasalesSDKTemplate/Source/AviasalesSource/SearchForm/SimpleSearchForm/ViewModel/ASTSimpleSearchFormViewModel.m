//
//  ASTSimpleSearchFormViewModel.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "ASTSimpleSearchFormViewModel.h"


@implementation ASTSimpleSearchFormCellViewModel

@end


@implementation ASTSimpleSearchFormAirportCellViewModel

@end


@implementation ASTSimpleSearchFormDateCellViewModel

@end


@implementation ASTSimpleSearchFormSectionViewModel

@end


@implementation ASTSimpleSearchFormPassengersViewModel

@end


@implementation ASTSimpleSearchFormViewModel

@end
