//
//  ASTComplexSearchFormFooterView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface ASTComplexSearchFormFooterView : UIView

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;

@property (nonatomic, copy) void (^addAction)(UIButton *sender);
@property (nonatomic, copy) void (^removeAction)(UIButton *sender);

@end
