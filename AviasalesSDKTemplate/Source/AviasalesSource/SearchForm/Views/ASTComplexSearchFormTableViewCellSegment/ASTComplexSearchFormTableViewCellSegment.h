//
//  ASTComplexSearchFormTableViewCellSegment.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface ASTComplexSearchFormTableViewCellSegment : UIView

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@property (nonatomic, copy) void (^tapAction)(UIView *sender);

@end
