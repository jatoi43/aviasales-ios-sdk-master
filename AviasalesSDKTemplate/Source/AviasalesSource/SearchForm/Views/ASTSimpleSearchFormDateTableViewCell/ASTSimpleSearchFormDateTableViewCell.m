//
//  ASTSimpleSearchFormDateTableViewCell.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "ASTSimpleSearchFormDateTableViewCell.h"

@implementation ASTSimpleSearchFormDateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.iconImageView transformRTL];
    self.iconImageView.tintColor = [JRColorScheme searchFormTintColor];
    self.returnButton.tintColor = [JRColorScheme searchFormTintColor];
    self.returnLabel.textColor = [JRColorScheme searchFormTintColor];
}

- (IBAction)returnButtonTapped:(UIButton *)sender {
    if (self.returnButtonAction) {
        self.returnButtonAction(sender);
    }
}

@end
