//
//  ASTSimpleSearchFormSeparatorView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ASTSearchFormSeparatorViewStyle) {
    ASTSearchFormSeparatorViewStyleTop,
    ASTSearchFormSeparatorViewStyleBottom
};

@interface ASTSimpleSearchFormSeparatorView : UIView

@property (nonatomic, strong) UIColor *separatorColor;
@property (nonatomic, assign) CGFloat leftInset;
@property (nonatomic, assign) CGFloat rightInset;
@property (nonatomic, assign) ASTSearchFormSeparatorViewStyle style;

@end
