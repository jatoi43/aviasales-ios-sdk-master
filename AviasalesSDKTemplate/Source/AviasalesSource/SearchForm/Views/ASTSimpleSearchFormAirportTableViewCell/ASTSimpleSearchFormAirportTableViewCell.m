//
//  ASTSimpleSearchFormAirportTableViewCell.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "ASTSimpleSearchFormAirportTableViewCell.h"

@implementation ASTSimpleSearchFormAirportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconImageView.tintColor = [JRColorScheme searchFormTintColor];
}

@end
