//
//  ASTComplexSearchFormTableViewCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "ASTComplexSearchFormTableViewCellSegment.h"

@interface ASTComplexSearchFormTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ASTComplexSearchFormTableViewCellSegment *origin;
@property (weak, nonatomic) IBOutlet ASTComplexSearchFormTableViewCellSegment *destination;
@property (weak, nonatomic) IBOutlet ASTComplexSearchFormTableViewCellSegment *departure;

@end
