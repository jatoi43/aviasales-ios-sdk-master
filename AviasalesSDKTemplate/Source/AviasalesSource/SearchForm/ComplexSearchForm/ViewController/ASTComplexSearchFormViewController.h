//
//  ASTComplexSearchFormViewController.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "ASTContainerSearchFormChildViewControllerProtocol.h"

@interface ASTComplexSearchFormViewController : UIViewController <ASTContainerSearchFormChildViewControllerProtocol>

@end
