//
//  ASTComplexSearchFormViewControllerProtocol.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

@class ASTComplexSearchFormViewModel;

@protocol ASTComplexSearchFormViewControllerProtocol <NSObject>

- (void)updateWithViewModel:(ASTComplexSearchFormViewModel *)viewModel;
- (void)addRowAnimatedAtIndex:(NSInteger)index withViewModel:(ASTComplexSearchFormViewModel *)viewModel;
- (void)removeRowAnimatedAtIndex:(NSInteger)index withViewModel:(ASTComplexSearchFormViewModel *)viewModel;
- (void)showAirportPickerWithType:(ASAirportPickerType)type forIndex:(NSInteger)index;
- (void)showDatePickerWithBorderDate:(NSDate *)borderDate selectedDate:(NSDate *)selectedDate forIndex:(NSInteger)index;
- (void)showPassengersPickerWithInfo:(ASTPassengersInfo *)passengersInfo;
- (void)showErrorWithMessage:(NSString *)message;
- (void)showWaitingScreenWithSearchInfo:(JRSDKSearchInfo *)searchInfo;

@end
