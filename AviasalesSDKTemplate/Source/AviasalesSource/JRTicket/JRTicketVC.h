//
//  JRTicketVC.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRViewController.h"

@interface JRTicketVC : JRViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

- (instancetype)initWithSearchInfo:(JRSDKSearchInfo *)searchInfo searchID:(NSString *)searchID;

- (void)setTicket:(JRSDKTicket *)ticket;

@end
