//
//  JRInfoPanelView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface JRInfoPanelView : UIView

@property (nonatomic, strong) JRSDKTicket *ticket;

@property (nonatomic, weak) IBOutlet UIButton *showOtherAgenciesButton;

@property (nonatomic, copy) void (^buyHandler)(void);
@property (nonatomic, copy) void (^showOtherAgencyHandler)(void);

- (void)expand;
- (void)collapse;

@end
