//
//  JRTransferCell.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTransferCell.h"
#import "DateUtil.h"

@implementation JRTransferCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.separatorInset = UIEdgeInsetsMake(0.0, self.bounds.size.width, 0.0, 0.0);
}

#pragma mark JRTicketCellProtocol methods

- (void)applyFlight:(JRSDKFlight *)nextFlight {
    NSString *delayString = [DateUtil duration:nextFlight.delay.integerValue durationStyle:JRDateUtilDurationShortStyle];
    self.durationLabel.text = [NSString stringWithFormat:@"%@: %@", AVIASALES_(@"JR_TICKET_TRANSFER"), delayString];
    self.placeLabel.text = [NSString stringWithFormat:@"%@ %@", nextFlight.originAirport.city, nextFlight.originAirport.iata];
}

@end
