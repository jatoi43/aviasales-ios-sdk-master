//
//  JRTicketCellProtocol.h
//
// Copyright 2019 Compare The Hotel limited
//

#ifndef ASTTicketCellProtocol_h
#define ASTTicketCellProtocol_h

@protocol JRTicketCellProtocol <NSObject>

- (void)applyFlight:(JRSDKFlight *)flight;

@end

#endif /* JRTicketCellProtocol_h */

