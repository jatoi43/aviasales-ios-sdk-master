//
//  JRFlightsSegmentHeaderView.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface JRFlightsSegmentHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) JRSDKFlightSegment *flightSegment;

@end
