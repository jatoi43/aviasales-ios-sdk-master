//
//  PriceCalendarChartHeader.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

class PriceCalendarChartHeader: UIView {
    
    @IBOutlet weak var chartContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        if let chartView = PriceCalendarChartView.loadFromNib() {

            chartContainerView.addSubview(chartView)
            chartView.translatesAutoresizingMaskIntoConstraints = false
            chartContainerView.addConstraints(JRConstraintsMakeScaleToFill(chartView, chartContainerView))

            chartView.reloadData()
            chartView.selectCurrentCenterCell()
        }
    }
}
