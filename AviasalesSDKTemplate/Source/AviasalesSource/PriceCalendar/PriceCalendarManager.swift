//
//  PriceCalendarManager.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

class PriceCalendarManager: NSObject {

    static let shared = PriceCalendarManager()

    private (set) var loader: JRSDKPriceCalendarLoader?
    private (set) var currency = CurrencyManager.shared.currency

    func prepareLoader(with searchInfo: JRSDKSearchInfo) {
        loader = JRSDKPriceCalendarLoader(searchInfo: searchInfo, delegate: nil)
        currency = CurrencyManager.shared.currency
    }
}
