//
//  ASTPassengersInfo.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel limited
//

import Foundation

@objcMembers
class ASTPassengersInfo: NSObject {

    let adults: Int
    let children: Int
    let infants: Int
    let travelClass: JRSDKTravelClass

    init(adults: Int, children: Int, infants: Int, travelClass: JRSDKTravelClass) {
        self.adults = adults
        self.children = children
        self.infants = infants
        self.travelClass = travelClass
        super.init()
    }
}
