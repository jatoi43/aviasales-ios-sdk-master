//
//  ASTPassengersPickerTableViewCell.swift
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

class ASTPassengersPickerTableViewCell: UITableViewCell {

    @IBOutlet weak var passengersTitleLabel: UILabel!
    @IBOutlet weak var passengersSubtitleLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!

    var minusAction: ((UIButton) -> Void)?
    var plusAction: ((UIButton) -> Void)?

    @IBAction func minusButtonTapped(_ sender: UIButton) {
        minusAction?(sender)
    }

    @IBAction func plusButtonTapped(_ sender: UIButton) {
        plusAction?(sender)
    }
}
