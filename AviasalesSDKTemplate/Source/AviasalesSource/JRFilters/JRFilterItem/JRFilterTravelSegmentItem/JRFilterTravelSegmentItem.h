//
//  JRFilterTravelSegmentItem.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterItemProtocol.h"


@class JRFilter;

@interface JRFilterTravelSegmentItem : NSObject <JRFilterItemProtocol>

@property (strong, nonatomic, readonly) JRSDKTravelSegment *travelSegment;

@property (nonatomic, copy) void (^filterAction)(void);

- (instancetype)initWithTravelSegment:(JRSDKTravelSegment *)travelSegment;

@end
