//
//  JRFilterListHeaderItem.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterItemProtocol.h"


@interface JRFilterListHeaderItem : NSObject <JRFilterItemProtocol> {
@protected
    NSInteger _itemsCount;
}

@property (nonatomic, assign) BOOL expanded;


- (instancetype)initWithItemsCount:(NSInteger)count;

@end


@interface JRFilterGatesHeaderItem : JRFilterListHeaderItem

@end


@interface JRFilterPaymentMethodsHeaderItem : JRFilterListHeaderItem

@end


@interface JRFilterAirlinesHeaderItem : JRFilterListHeaderItem

@end


@interface JRFilterAllianceHeaderItem : JRFilterListHeaderItem

@end


@interface JRFilterAirportsHeaderItem : JRFilterListHeaderItem

@end
