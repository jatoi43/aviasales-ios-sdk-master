//
//  JRFilterItemProtocol.h
//
// Copyright 2019 Compare The Hotel limited
//


#import <Foundation/Foundation.h>


@protocol JRFilterItemProtocol <NSObject>

- (NSString *)title;

@optional

@property (nonatomic, copy) void (^filterAction)(void);

- (NSString *)detailsTitle;
- (NSAttributedString *)attributedStringValue;

@end

