//
//  JRFilterListSeparatorItem.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterListSeparatorItem.h"


@implementation JRFilterListSeparatorItem {
    NSString *_title;
}

- (instancetype)initWithTitle:(NSString *)title {
    self = [super init];
    if (self) {
        _title = [title copy];
    }
    
    return self;
}

- (NSString *)title {
    return _title;
}

@end
