//
//  JRFilterListSeparatorItem.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterItemProtocol.h"


@interface JRFilterListSeparatorItem : NSObject <JRFilterItemProtocol>

- (instancetype)initWithTitle:(NSString *)title;

@end
