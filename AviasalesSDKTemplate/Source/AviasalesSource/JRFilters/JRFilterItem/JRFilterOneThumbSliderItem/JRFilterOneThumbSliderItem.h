//
//  JRFilterOneThumbSliderItem.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterItemProtocol.h"


@interface JRFilterOneThumbSliderItem : NSObject <JRFilterItemProtocol>

@property (nonatomic, assign, readonly) CGFloat minValue;
@property (nonatomic, assign, readonly) CGFloat maxValue;

@property (nonatomic, assign) CGFloat currentValue;

@property (nonatomic, copy) void (^filterAction)(void);

- (instancetype)initWithMinValue:(CGFloat)minValue maxValue:(CGFloat)maxValue currentValue:(CGFloat)currentValue;

@end


@interface JRFilterPriceItem : JRFilterOneThumbSliderItem

@end


@interface JRFilterTotalDurationItem : JRFilterOneThumbSliderItem

@end
