//
//  JRFilterItemsFactory.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>
#include "JRFilterItemProtocol.h"

@class JRFilter;
@class JRFilterItem;


@interface JRFilterItemsFactory : NSObject

- (instancetype)initWithFilter:(JRFilter *)filter;

- (NSArray *)createSectionsForSimpleMode;
- (NSArray *)createSectionsForComplexMode;
- (NSArray *)createSectionsForTravelSegment:(JRSDKTravelSegment *)travelSegment;

@end
