//
//  JRFilterCellsFactory.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

#import "JRFilterVC.h"

@class JRTableViewCell;
@class JRFilterItem;

@interface JRFilterCellsFactory : NSObject

- (nonnull instancetype)initWithTableView:(nonnull UITableView *)tableView withFilterMode:(JRFilterMode)mode;

- (nonnull UITableViewCell *)cellByItem:(nonnull JRFilterItem *)item;
- (CGFloat)heightForCellByItem:(nonnull JRFilterItem *)item;

@end
