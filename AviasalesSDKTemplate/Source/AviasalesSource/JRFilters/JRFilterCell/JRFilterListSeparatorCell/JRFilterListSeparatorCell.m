//
//  JRFilterListSeparatorCell.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterListSeparatorCell.h"

#import "JRFilterListSeparatorItem.h"
#import "JRColorScheme.h"

@interface JRFilterListSeparatorCell ()

@end


@implementation JRFilterListSeparatorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = self.contentView.backgroundColor = [JRColorScheme mainColor];
}

- (void)setItem:(JRFilterListSeparatorItem *)item {
    _item = item;
    
    self.separatorLabel.text = [item title];
}

@end
