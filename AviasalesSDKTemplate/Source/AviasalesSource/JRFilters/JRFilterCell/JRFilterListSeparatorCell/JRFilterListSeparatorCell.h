//
//  JRFilterListSeparatorCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRTableViewCell.h"

@class JRFilterListSeparatorItem;

@interface JRFilterListSeparatorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *separatorLabel;

@property (strong, nonatomic) JRFilterListSeparatorItem *item;

@end
