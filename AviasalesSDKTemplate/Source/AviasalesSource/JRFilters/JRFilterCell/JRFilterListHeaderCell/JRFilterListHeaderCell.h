//
//  JRFilterListHeaderCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>
#import "JRFilterListHeaderItem.h"
#import "JRTableViewCell.h"

@interface JRFilterListHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UIImageView *openIndicator;
@property (weak, nonatomic) IBOutlet UIView *alphaView;

@property (strong, nonatomic) JRFilterListHeaderItem *item;

@property (nonatomic, assign) BOOL expanded;

- (void)setExpanded:(BOOL)expanded animated:(BOOL)animated;

@end
