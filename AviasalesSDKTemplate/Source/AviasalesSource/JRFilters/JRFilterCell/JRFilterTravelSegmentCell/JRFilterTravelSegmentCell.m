//
//  JRFilterTravelSegmentCell.m
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterTravelSegmentCell.h"

#import "JRFilterTravelSegmentItem.h"


@implementation JRFilterTravelSegmentCell

- (void)setItem:(JRFilterTravelSegmentItem *)item {
    _item = item;
    
    self.flightDirectionLabel.text = [item title];
    self.deparureDateLabel.text = [item detailsTitle];
}

@end
