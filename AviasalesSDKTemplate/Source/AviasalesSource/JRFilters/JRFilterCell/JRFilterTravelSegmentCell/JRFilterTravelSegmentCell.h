//
//  JRFilterTravelSegmentCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTableViewCell.h"

@class JRFilterTravelSegmentItem;


@interface JRFilterTravelSegmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *flightDirectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparureDateLabel;

@property (strong, nonatomic) JRFilterTravelSegmentItem *item;

@end
