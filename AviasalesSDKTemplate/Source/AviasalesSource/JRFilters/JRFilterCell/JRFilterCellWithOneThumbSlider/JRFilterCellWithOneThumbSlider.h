//
//  ASFilterCellWithOneThumbSlider.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTableViewCell.h"

@class JRFilterOneThumbSliderItem;

@interface JRFilterCellWithOneThumbSlider : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellAttLabel;
@property (weak, nonatomic) IBOutlet UISlider *cellSlider;

@property (strong, nonatomic) JRFilterOneThumbSliderItem *item;

@end
