//
//  JRFilterListCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTableViewCell.h"

@class JRFilterCheckBoxItem;

@interface JRFilterCheckboxCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectedIndicator;
@property (weak, nonatomic) IBOutlet UILabel *listItemLabel;
@property (weak, nonatomic) IBOutlet UILabel *listItemDetailLabel;

@property (nonatomic, strong) JRFilterCheckBoxItem *item;

@property (nonatomic, assign) BOOL checked;

@end
