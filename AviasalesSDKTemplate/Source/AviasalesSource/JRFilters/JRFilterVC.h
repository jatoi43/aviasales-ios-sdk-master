//
//  JRFilterVC.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRViewController.h"

typedef NS_ENUM (NSUInteger, JRFilterMode) {
    JRFilterComplexMode         = 0,
    JRFilterSimpleSearchMode    = 1,
    JRFilterTravelSegmentMode   = 2
};


@class JRFilter;


@interface JRFilterVC : JRViewController

- (instancetype)initWithFilter:(JRFilter *)filter
                 forFilterMode:(JRFilterMode)filterMode
         selectedTravelSegment:(JRSDKTravelSegment *)travelSegment;

@end
