//
//  JRFilterTask.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRFilterTicketBounds.h"


@protocol JRFilterTaskDelegate<NSObject>

@required
- (void)filterTaskDidFinishWithTickets:(NSOrderedSet<JRSDKTicket *> *)filteredTickets;

@end


@interface JRFilterTask : NSObject

+ (instancetype)filterTaskForTickets:(NSOrderedSet<JRSDKTicket *> *)ticketsToFilter
                        ticketBounds:(JRFilterTicketBounds *)ticketBounds
                 travelSegmentBounds:(NSArray *)travelSegmentBounds
                        taskDelegate:(id<JRFilterTaskDelegate>)delegate;

- (void)performFiltering;

@end
