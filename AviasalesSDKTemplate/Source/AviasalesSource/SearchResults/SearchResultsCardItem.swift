//
//  SearchResultsCardItem.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

class SearchResultsCardItem: NSObject {
    let index: Int
    let view: UIView
    let height: CGFloat

    init(index: Int, view: UIView, height: CGFloat) {
        self.index = index
        self.view = view
        self.height = height
        super.init()
    }
}
