//
//  JRSearchResultsList.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRTableManager.h"

@class JRSearchResultsFlightSegmentCellLayoutParameters;

@protocol JRSearchResultsListDelegate <NSObject>

- (NSArray<JRSDKTicket *> *)tickets;
- (void)didSelectTicketAtIndex:(NSInteger)index;

@end


@interface JRSearchResultsList : NSObject <JRTableManager>

@property (weak, nonatomic) id<JRSearchResultsListDelegate> delegate;
@property (strong, nonatomic, readonly) NSString *ticketCellNibName;
@property (strong, nonatomic) JRSearchResultsFlightSegmentCellLayoutParameters *flightSegmentLayoutParameters;

- (instancetype)initWithCellNibName:(NSString *)cellNibName;

@end
