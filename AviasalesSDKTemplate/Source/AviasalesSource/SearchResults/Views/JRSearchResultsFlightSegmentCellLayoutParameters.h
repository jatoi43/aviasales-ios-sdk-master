//
//  JRSearchResultsFlightSegmentCellLayoutParameters.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <Foundation/Foundation.h>

@interface JRSearchResultsFlightSegmentCellLayoutParameters : NSObject
@property (assign, nonatomic, readonly) CGFloat departureDateWidth;
@property (assign, nonatomic, readonly) CGFloat departureLabelWidth;
@property (assign, nonatomic, readonly) CGFloat arrivalLabelWidth;
@property (assign, nonatomic, readonly) CGFloat flightDurationWidth;

- (instancetype)initWithDepartureDateWidth:(CGFloat)departureDateWidth
                       departureLabelWidth:(CGFloat)departureLabelWidth
                         arrivalLabelWidth:(CGFloat)arrivalLabelWidth
                       flightDurationWidth:(CGFloat)flightDurationWidth;

+ (instancetype)parametersWithTickets:(NSArray<JRSDKTicket *> *)tickets
                                 font:(UIFont *)font;
@end
