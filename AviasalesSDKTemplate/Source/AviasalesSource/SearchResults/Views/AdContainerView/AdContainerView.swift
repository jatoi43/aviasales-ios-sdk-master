//
//  AdContainerView.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

class AdContainerView: UIView {

    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = JRColorScheme.mainBackgroundColor()
        containerView.layer.cornerRadius = 6
    }
}
