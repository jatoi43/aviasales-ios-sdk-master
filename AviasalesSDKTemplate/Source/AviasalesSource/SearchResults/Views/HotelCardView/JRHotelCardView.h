//
//  JRHotelCardView.h
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

#import <UIKit/UIKit.h>

@interface JRHotelCardView : UIView

@property (nonatomic, copy) void (^buttonAction)(void);

@end
