//
//  JRResultsTicketCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@class JRSearchResultsFlightSegmentCellLayoutParameters;

@interface JRResultsTicketCell : UITableViewCell

@property (strong, nonatomic) JRSDKTicket *ticket;
@property (strong, nonatomic) JRSearchResultsFlightSegmentCellLayoutParameters *flightSegmentsLayoutParameters;

+ (NSString *)nibFileName;
+ (CGFloat)heightWithTicket:(JRSDKTicket *)ticket;

@end
