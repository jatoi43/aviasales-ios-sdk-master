//
//  JRResultsTicketPriceCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@interface JRResultsTicketPriceCell : UITableViewCell

@property (strong, nonatomic) JRSDKPrice *price;
@property (strong, nonatomic) JRSDKAirline *airline;

+ (NSString *)nibFileName;
+ (CGFloat)height;

@end
