//
//  JRResultsFlightSegmentCell.h
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

@protocol JRSDKFlightSegment;
@class JRSearchResultsFlightSegmentCellLayoutParameters;

@interface JRResultsFlightSegmentCell : UITableViewCell

@property (strong, nonatomic) JRSDKFlightSegment *flightSegment;
@property (strong, nonatomic) JRSearchResultsFlightSegmentCellLayoutParameters *layoutParameters;

+ (NSString *)nibFileName;
+ (CGFloat)height;

@end
