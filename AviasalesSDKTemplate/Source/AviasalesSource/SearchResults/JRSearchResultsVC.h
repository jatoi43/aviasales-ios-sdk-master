//
//  JRSearchResultsVC.h
//
// Copyright 2019 Compare The Hotel limited
//

#import "JRViewController.h"
#import "JRFilterVC.h"


@interface JRSearchResultsVC : JRViewController

- (instancetype)initWithSearchInfo:(JRSDKSearchInfo *)searchInfo response:(JRSDKSearchResult *)response;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, copy) void (^selectionBlock)(JRSDKTicket *selectedTicket);
@property (nonatomic, copy) void (^filterChangedBlock)(BOOL isEmptyResults);

@end
