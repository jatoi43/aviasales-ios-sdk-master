//
//  ASAirportPickerDataCell.swift
//  CompareTheHotel
//
//
//  Copyright © 2019 Compare The Hotel Limited.
//

import UIKit

protocol ASAirportPickerDataCellProtocol {
    var city: String? { get }
    var airport: String? { get }
    var iata: String { get }
}

class ASAirportPickerDataCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var airportLabel: UILabel!
    @IBOutlet weak var iataLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        cityLabel.textColor = JRColorScheme.darkTextColor()
        airportLabel.textColor = JRColorScheme.lightTextColor()
        iataLabel.textColor = JRColorScheme.darkTextColor()
    }

    func setup(cellModel: ASAirportPickerDataCellProtocol) {
        cityLabel.text = cellModel.city
        airportLabel.text = cellModel.airport
        iataLabel.text = cellModel.iata
    }
}
