//
//  ASAirportPickerDataCellModel.swift
//  CompareTheHotel
//
//
//  Copyright © 2019 Compare The Hotel Limited.
//

struct ASAirportPickerDataCellModel: ASAirportPickerCellModelProtocol, ASAirportPickerDataCellProtocol {
    let type = ASAirportPickerCellModelType.data
    let city: String?
    let airport: String?
    let iata: String
    let model: JRSDKAirport
}
