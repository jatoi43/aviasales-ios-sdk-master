//
//  ASAirportPickerSectionView.swift
//  CompareTheHotel
//
//
//  Copyright © 2019 Compare The Hotel Limited.
//

import UIKit

protocol ASAirportPickerSectionViewProtocol {
    var name: String? { get }
}

class ASAirportPickerSectionView: UIView {

    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.textColor = JRColorScheme.darkTextColor()
    }

    func setup(sectionModel: ASAirportPickerSectionViewProtocol) {
        nameLabel.text = sectionModel.name
    }
}
