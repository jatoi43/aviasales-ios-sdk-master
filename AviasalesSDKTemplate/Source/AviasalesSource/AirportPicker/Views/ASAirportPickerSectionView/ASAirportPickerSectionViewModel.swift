//
//  ASAirportPickerSectionViewModel.swift
//  CompareTheHotel
//
//  
//  Copyright © 2019 Compare The Hotel Limited.
//

enum ASAirportPickerCellModelType {
    case info
    case data
}

protocol ASAirportPickerCellModelProtocol {
    var type: ASAirportPickerCellModelType { get }
}

struct ASAirportPickerSectionViewModel: ASAirportPickerSectionViewProtocol {
    let name: String?
    let cellModels: [ASAirportPickerCellModelProtocol]
}
