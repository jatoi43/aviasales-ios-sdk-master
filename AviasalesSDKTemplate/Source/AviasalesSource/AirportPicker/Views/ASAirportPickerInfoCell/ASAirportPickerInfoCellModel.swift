//
//  ASAirportPickerInfoCellModel.swift
//  CompareTheHotel
//
//
//  Copyright © 2019 Compare The Hotel Limited.
//

struct ASAirportPickerInfoCellModel: ASAirportPickerCellModelProtocol, ASAirportPickerInfoCellProtocol {
    let type = ASAirportPickerCellModelType.info
    let loading: Bool
    let info: String
}
