//
//  UIView+ShadowLayer.swift
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

@objc extension UIView {

    func applyShadowLayer() {
        let cornerRadius: CGFloat = 6
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 7.0
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
}
