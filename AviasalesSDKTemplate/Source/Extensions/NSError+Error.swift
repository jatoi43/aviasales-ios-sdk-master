//
//  NSError+Error.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension NSError {

    @objc var isCancelled: Bool {
        return (self as Error).isCancelled
    }
}
