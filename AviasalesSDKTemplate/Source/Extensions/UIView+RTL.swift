//
//  UIView+RTL.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension UIView {

    var isRTL: Bool {
        return UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft
    }

    @objc func transformRTL() {
        if isRTL {
            transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

}
