//
//  NSString+Digits.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension NSString {

    private static let digits: [String : String] = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "ar")
        var digits = [String : String]()
        Array(0...9).forEach { value in
            if let key = formatter.string(from: NSNumber(value: value)) {
                digits[key] = "\(value)"
            }
        }
        return digits
    }()

    @objc func arabicDigits() -> String {
        return (self as String).map { NSString.digits["\($0)"] ?? "\($0)" }.joined()
    }
}
