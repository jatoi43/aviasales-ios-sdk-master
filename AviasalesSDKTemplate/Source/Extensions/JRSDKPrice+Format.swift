//
//  JRSDKPrice+Format.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension JRSDKPrice {

    @objc func formattedPriceinUserCurrency() -> String {
        let price = AviasalesNumberUtil.formatPrice(priceInUserCurrency()) ?? ""
        return price.arabicDigits().rtlStringIfNeeded()
    }
}
