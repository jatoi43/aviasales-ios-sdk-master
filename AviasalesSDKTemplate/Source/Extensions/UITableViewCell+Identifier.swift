//
//  UITableViewCell+Identifier.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import UIKit

extension UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
}
