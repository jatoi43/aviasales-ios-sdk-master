//
//  NSNumber+Timestamp.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension NSNumber {

    func formattedTimestampRepresentation(formatter: DateFormatter) -> String {
        let date = Date(timeIntervalSince1970: self.doubleValue)
        return formatter.string(from: date)
    }
}
