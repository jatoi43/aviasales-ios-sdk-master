//
//  HLSearchInfoType+Description.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension HLSearchInfoType {

    func description() -> String {
        switch self {
        case .unknown:
            return "Unknown"
        case .city:
            return "City"
        case .hotel:
            return "Hotel"
        case .userLocation:
            return "User Location"
        case .cityCenterLocation:
            return "City Center Location"
        case .airport:
            return "Airport"
        case .customLocation:
            return "Custom Location"
        }
    }
}
