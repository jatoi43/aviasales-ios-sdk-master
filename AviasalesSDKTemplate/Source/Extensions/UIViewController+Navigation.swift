//
//  UIViewController+Navigation.swift
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

@objc extension UIViewController {

    func pushOrPresentBasedOnDeviceType(viewController: UIViewController, animated: Bool) {
        if iPhone() {
            navigationController?.pushViewController(viewController, animated: animated)
        } else {
            let navigationController = JRNavigationController(rootViewController: viewController)
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLS("JR_CLOSE_BUTTON_TITLE"), style: .plain, target: self, action:  #selector(dismissViewController))
            navigationController.modalPresentationStyle = .formSheet
            present(navigationController, animated: animated, completion: nil)
        }
    }

    @objc func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }

    func popOrDismissBasedOnDeviceType(animated: Bool) {
        if iPhone() {
            navigationController?.popViewController(animated: animated)
        } else {
            navigationController?.dismiss(animated: animated, completion: nil)
        }
    }
}
