//
//  WKWebView+Scripts.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import WebKit

extension WKWebView {

    convenience init(scripts: [String]) {
        let userContentController = WKUserContentController()
        scripts.forEach { (script) in
            let userScript = WKUserScript.init(source: script, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
            userContentController.addUserScript(userScript)
        }
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
        self.init(frame: .zero, configuration: configuration)
    }
}
