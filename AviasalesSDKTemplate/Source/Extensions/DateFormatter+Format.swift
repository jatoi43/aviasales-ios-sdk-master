//
//  DateFormatter+Format.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

import Foundation

extension DateFormatter {

    convenience init(format: String) {
        self.init()
        self.dateFormat = format
    }
}
