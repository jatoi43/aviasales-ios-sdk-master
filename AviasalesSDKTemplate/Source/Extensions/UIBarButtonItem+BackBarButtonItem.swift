//
//  UIBarButtonItem+BackBarButtonItem.swift
//
// Copyright 2019 Compare The Hotel limited
//

import UIKit

@objc extension UIBarButtonItem {

    static func backBarButtonItem() -> UIBarButtonItem {
        return UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
    }
}
