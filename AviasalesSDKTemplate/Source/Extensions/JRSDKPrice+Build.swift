//
//  JRSDKPrice+Build.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension JRSDKPrice {

    @objc static func price(currency: String, value: Float) -> JRSDKPrice? {
        let builder = JRSDKPriceBuilder()
        builder.currency = currency
        builder.value = value
        return builder.build()
    }
}
