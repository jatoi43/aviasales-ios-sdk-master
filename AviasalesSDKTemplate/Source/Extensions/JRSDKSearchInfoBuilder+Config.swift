//
//  JRSDKSearchInfoBuilder+Config.swift
//  CompareTheHotel
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension JRSDKSearchInfoBuilder {

    static func buildTravelSegmentsBasedOnConfig() -> NSOrderedSet? {

        guard let origin = ConfigManager.shared.flightsOrigin, let destination = ConfigManager.shared.flightsDestination else {
            return nil
        }

        let airportsStorage = AviasalesSDK.sharedInstance().airportsStorage

        let travelSegmentBuilder = JRSDKTravelSegmentBuilder()

        travelSegmentBuilder.originAirport = airportsStorage.findAnything(byIATA: origin)
        travelSegmentBuilder.destinationAirport = airportsStorage.findAnything(byIATA: destination)
        travelSegmentBuilder.departureDate = DateUtil.nextWeekend()

        guard let travelSegment = travelSegmentBuilder.build() else {
            return nil
        }

        return NSOrderedSet(object: travelSegment)
    }
}
