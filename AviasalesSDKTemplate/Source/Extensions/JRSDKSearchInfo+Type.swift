//
//  JRSDKSearchInfo+Type.swift
//  CompareTheHotel
//
//
// Copyright 2019 Compare The Hotel Ltd.
//

extension JRSDKSearchInfo {

    func typeRepresentation() -> String {
        switch JRSDKModelUtils.searchInfoType(self) {
        case .oneWayType:
            return "Oneway"
        case .directReturnType:
            return "Return"
        case .complexType:
            return "Multicity"
        }
    }
}
