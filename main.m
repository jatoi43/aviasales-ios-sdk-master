//
//  main.m
//
// Copyright 2019 Compare The Hotel limited
//

#import <UIKit/UIKit.h>

#import "JRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JRAppDelegate class]));
    }
}
